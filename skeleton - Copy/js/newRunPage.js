
class Location{
    
    constructon(page){
        this._page = page;
        console.log(page);
        this.distance = 0;
		
    }
    
    /**
     * Gets a random location from randomDestination function and adds it to the map
    */
    addRandomLocation()
    {
        let geoLocation = this._page.timeSnap.currentLocation;
        
        //Gets a random destination
        let randomLocation = this.randomDestination (geoLocation);

        this._page.runRecorder.run1.addToLandMarks(randomLocation);
        
        //Mark it on the map
        this._page.map.addMarker(randomLocation);
        
        //Enable the run start button
        document.getElementById("start run-button").disabled = false;

    }
    
    /**
     * Generates a random destination within 60m - 150m
     * @param {mapboxgl.LngLat} startLocation - The start location of the run
    */
    randomDestination (startLocation) 
    {	
        let distance = 0;

        //The start location of the run is used as a base to generate a random destination
        let modifiedLocation = startLocation;

        while(distance < 60){

            //The range of the final destination with relation to the initialLocation
            let range = parseFloat((Math.random() * (0.001000 - 0.000001) + 0.000001).toFixed(6));

            //Adds or substract a range from a value
            function addOrSub(value, range){
                let addOrSub = Math.floor(Math.random() * 2) + 1;
                return value + ((-1)**addOrSub)*range;
            }

            //Add or substract range from start longitude and latitude
            let newLng = addOrSub(startLocation.lng, range);
            let newLat = addOrSub(startLocation.lat, range);

            //Generate the new modifiedLocation as end location of run
            var arr = [newLng, newLat];
            modifiedLocation = mapboxgl.LngLat.convert(arr);

            //Get the distance to the modifiedLocation from the start location
            distance = modifiedLocation.distanceTo(startLocation);
            
        }
        
        this.distance=distance;
        return modifiedLocation;
		
    }
    
    set page(page){
        this._page = page;
    }
}

class RunRecorder{
    
    constructor(page){
        
        this._page = page;
        
        //Stores the run's start time
        this.recordStartTime = undefined;

        //Stores the run's end time
        this.recordEndTime = undefined;

        //The init location of the run
        this.initialLocation = undefined;

        //The final location of the run
        this.destinationLocation = undefined;

        //Time now - recordStartTime
        this.timeLapsed = undefined;

        //To know route of run
        this.landMarks = [];

        // Initially, the recording of time has not started. Hence, false.
        this.recordingHasStarted = false;

        //The run which is currently being recorded
        //Instantiate a new run
        this.run1=new Run();
        
        //Setting an onclick listener to the start run button
        document.getElementById("start run-button").onclick = this.startRun.bind(this);
    }
    
    /**DOES NOT - Run Recorder
     * Starts the new run.
    */
    startRun()
    {
        //Record the initial location
        this.initialLocation=this._page.timeSnap.currentLocation;
        this.recordStartTime=new Date();
        this.timeLapsed=0;
        this.recordingHasStarted=true;
        this.landMarks.push(this.initialLocation);

    }
	
	endRun()
	{
	   this.destinationLocation=this._page.location.destinationLocation;
	   this.recordEndTime=new Date();
	   this.timeLapsed=0;
	   this.recordingHasStarted=false;
	   this.landMarks.push(this.destinationLocation);
	}
    
    /**
     * Funtion to call to update location(among other things) while running
     * @param {mapboxgl.LngLat} currentLngLat
    */
    updateWhileRunning(currentLngLat){
        
        
        
    }
    
    set page(page){
        this._page = page;
    }
}

class Page{
    constructor(){
        this.map = new Map(this);
        //this.timeSnap = new LocationClicker(this);
        this.timeSnap = new LocationUpdater(this);
        this.location = new Location(this);
        this.runRecorder = new RunRecorder(this);
    }
    
    setPage(){
        this.timeSnap.page = this;
        this.location.page = this;
        this.runRecorder.page = this;
    }
    
    /**
     * gets a random location when some button is clicked. WHICH BUTTON?
    */
    buttonClick()
    {
        //Registers the current location
        page.location.addRandomLocation();
		document.getElementById("p1").innerHTML = "The Estimated(assuming Direct Path is taken)distance betweent the initial Location and destination Location is: " + page.location.distance + "m";

		//console.log(page.location.distance);
    }

}

let page = new Page();
page.setPage();



